package app.ecommerce.ui.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

import app.ecommerce.ui.R;
import app.ecommerce.ui.adapter.ActivityDayPlansAdapter;
import app.ecommerce.ui.adapter.ExampleAdapter;
import app.ecommerce.ui.data.DataGenerator;
import app.ecommerce.ui.model.ExampleProduct;
import app.ecommerce.ui.utils.Tools;

public class ActivityDayPlans extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_plans);

       initData();

        initToolbar();

        initRecyclerView();




    }

    void initData(){
        final LinearLayout hiddenBar = findViewById(R.id.hiddenBar);
        final LinearLayout hiddenBar1 = findViewById(R.id.hiddenBara);
        final ImageView klikArrow = findViewById(R.id.klikArrow);
        final CardView cardView = findViewById(R.id.cardView);


        hiddenBar.setVisibility(View.GONE);
        hiddenBar1.setVisibility(View.GONE);


        klikArrow.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                if (hiddenBar.getVisibility()==View.GONE){
                    TransitionManager.beginDelayedTransition(cardView, new AutoTransition());
                    hiddenBar.setVisibility(View.VISIBLE);
                    klikArrow.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                } else {
                    TransitionManager.beginDelayedTransition(cardView, new AutoTransition());
                    hiddenBar.setVisibility(View.GONE);
                    klikArrow.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }
            }
        });
    }

    public void initToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Products");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);

    }

    void initRecyclerView(){
        // define recycler view
        RecyclerView recycler_view = findViewById(R.id.rv1);
        recycler_view.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        recycler_view.setHasFixedSize(true);
        recycler_view.setNestedScrollingEnabled(false);

        List<ExampleProduct> items = DataGenerator.getProducts(this);
        items.addAll(DataGenerator.getProducts(this));
        items.addAll(DataGenerator.getProducts(this));
        items.addAll(DataGenerator.getProducts(this));

        //set data and list adapter
        ActivityDayPlansAdapter adapter = new ActivityDayPlansAdapter(this, items);
        recycler_view.setAdapter(adapter);

        // on item list clicked
        adapter.setOnItemClickListener(new ExampleAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, ExampleProduct obj, int position) {
                Toast.makeText(getApplicationContext(), "Item " + obj.title + " clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.example_menu, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.grey_60));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

}
